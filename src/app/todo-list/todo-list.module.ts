import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormComponent } from './form/form.component';
import { HeaderComponent } from './header/header.component';
import { NoTaskFoundComponent } from './no-task-found/no-task-found.component';
import { TaskItemComponent } from './task-item/task-item.component';
import { TotalTaskCountComponent } from './total-task-count/total-task-count.component';

@NgModule({
  declarations: [
    HeaderComponent,
    TaskItemComponent,
    NoTaskFoundComponent,
    TotalTaskCountComponent,
    FormComponent,
  ],
  imports: [CommonModule],
  exports: [
    HeaderComponent,
    TaskItemComponent,
    NoTaskFoundComponent,
    TotalTaskCountComponent,
    FormComponent,
  ]
})
export class TodoListModule {}
