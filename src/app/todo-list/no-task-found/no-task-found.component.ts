import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-no-task-found',
  templateUrl: './no-task-found.component.html',
  styleUrls: ['./no-task-found.component.scss']
})
export class NoTaskFoundComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
