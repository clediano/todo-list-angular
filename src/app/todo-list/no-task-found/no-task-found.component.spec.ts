import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoTaskFoundComponent } from './no-task-found.component';

describe('NoTaskFoundComponent', () => {
  let component: NoTaskFoundComponent;
  let fixture: ComponentFixture<NoTaskFoundComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoTaskFoundComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(NoTaskFoundComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
