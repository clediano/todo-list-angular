import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalTaskCountComponent } from './total-task-count.component';

describe('TotalTaskCountComponent', () => {
  let component: TotalTaskCountComponent;
  let fixture: ComponentFixture<TotalTaskCountComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TotalTaskCountComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(TotalTaskCountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
